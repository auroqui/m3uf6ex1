/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m3_uf6_exercici1;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author auroq
 */
public class M3_UF6_Exercici1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Connectar a la base de dades

        DataSource ds = getMysqlDataSource();
        Connection conn;
        try {
            conn = ds.getConnection();
            
            conn.close();
            //Executar una sentència SQL
        } catch (SQLException ex) {
            System.out.println("FAIL");
            Logger.getLogger(M3_UF6_Exercici1.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }

    public static DataSource getMysqlDataSource() {
        oracle.jdbc.pool.OracleDataSource oracleDS = null;

        try {
            oracleDS = new oracle.jdbc.pool.OracleDataSource();

            oracleDS.setURL("jdbc:oracle:thin:@ieslaferreria.xtec.cat:8081:INSLAFERRERI");
            oracleDS.setUser("EOS_2018");
            oracleDS.setPassword("1234");
            
            

        } catch (Exception e) {

        }

        return oracleDS;

    }
    
    

}
